# Stack Traefik et Portainer pour la gestion de l'environnement de dev (pour Alpes Isère Habitat) 

Ce stack à pour objectif de fournir un environnement de gestion de nos conteneurs Docker avec Traefik et Portainer.
L'usage de ce stack est à destination essentiellement des phases de développement de Alpes Isère Habitat.

## Composants du stack
* traefik:v2.0
* portainer

## Installation
L'installation du stack ce réalise dans l'ordre suivant :
* Cloner ce projet Git sur votre machine
* Entrer dans le repertoire


```bash
git clone hhttps://gitlab.com/yoan.bernabeu/traefik-portainer.git
cd cd traefik-portainer
```

## Usage

Création du Network (la première fois)
```docker-compose
docker network create web
```

Lancer le stack
```docker-compose
docker-compose up -d
```

Afficher la liste des conteneurs
```docker-compose
docker-compose ps
```

Ajouter un service (remplacer nom par vos infos) d'un autre docker-compose.yml
```bash
version: '3'
services:
    nom:
        container_name: nom
        image: nom/nom
        labels:
          - "traefik.http.routers.nom.rule=Host(`nom.localhost`)"
          - "traefik.http.services.nom.loadbalancer.server.port=XX"

        networks:
          - web
networks:
  web:
    external:
      name: web
```

## Auteurs
* Yoan Bernabeu pour Alpes isère Habitat 
